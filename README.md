# GUI for Accessing LabRemote Equipment (GALE)

### Graphic Interface for intended use with ATLAS Electrical Testing

##### Developed by Hava Schwartz at the Santa Cruz Institute for Particle Physics (SCIPP)

#### App Description:

This app was created to remotely control hardware lab equipment, such as power supplies and chillers. It was orginally designed to be used in ATLAS Inner Tracker Pixel Module Testing, but it can have more broad applications. GALE meets the need of having a reliable and user-friendly method for controlling hardware equipment, alternative to the command line. In turn, implementation of this app can make a lab setup more inclusive to non-technical users without the risk of human error.

FastAPI was selected for building the backend, since it is one of the fastest Python frameworks available. The backend will route html requests to python functions, which will send hardware commands. The app relies on LabRemote libraries and provides a graphic user interface for accessing LabRemote control features. It is therefore compatible with a diverse range of lab equipment and models, and LabRemote libraries continues to be developed, expanding the specific model selection available. 

The frontend graphics have been designed using React Javascript Library, which streamlines updates to the display through its method of rendering internal state data. The code is split into individual components written in JavaScript-XML Notation (JSX), allowing for ease of interpretation and customization.

The app has been dockerized, making it compatible with any operating system or computing environment. It must be run on a computer which is physically hard-wired to the lab equipment, but can be accessed on a local computer through remote connection with the lab setup.

#### Requirements for Using this App:

- App should be loaded on the computer that is physically hardwired to lab equipment
- Lab hardware should be accessible via labRemote libraries, and appropriate JSON config files should be generated
- Docker installed

#### Cloning Repository

- Clone GitLab Repository

```
git clone https://gitlab.cern.ch/scipp/pixels/gale.git
```

#### Building Docker Images:

If you prefer not to use docker, skip to "Installing Dependencies without using Docker".

- Navigate to backend

```
cd gale
cd backend
```

- Build backend docker image

```
docker build -t fastapi:dev .
```

- Navigate to frontend

```
cd ../frontend
```

- Build frontend docker image

```
docker build -t react:dev .
```

#### Running App with Docker

- Navigate to root directory

```
cd ..
```

- Simultaneously run both docker containers in detached mode using compose

```
docker compose up -d
```

- Open a browser and point it to [localhost:3000](localhost:3000)

#### Installing Dependencies without using Docker:

- Navigate to backend

```
cd gale
cd backend
```

- Generate and source a virtual environment

```
python3 -m venv gale_venv
source gale_venv/bin/activate
export PYTHONPATH=$PWD
```

- Install FastAPI and its dependencies

```
pip install fastapi==0.61.1 uvicorn==0.11.8
pip install numpy
```

- Navigate to frontend

```
cd ../frontend
```

- Install React Dependencies

```
npm install @chakra-ui/core @emotion/core @emotion/styled emotion-theming
npm install --save react-tabs
npm install react-router-dom
```

#### Running App

- Launch Backend

```
cd gale/backend
source gale_venv/bin/activate
python3 main.py
```

- Launch Frontend (In a new terminal)

```
cd gale/frontend
npm run start
```

This should launch a browser window, pointing to the App.

#### Generating labRemote Config JSON

This App is dependent on labRemote libraries. If a labRemote config file is already in use for your hardware, then just be sure that it has the required categories. If one is not being used, it can be easily created by following the examples in: gale/backend/configs/

Once the labRemote config JSONs are created, they should be stored in gale/backend/configs/, where the API will be able to access them and render them into the display. Any hardware device should only be integrated into a single labRemote config on any given computer. Make sure that no other programs or terminals are attempting to access the same device.

**Required Categories for Power Supply labRemote Config:**

- "devices"
  - "name"
  - "communication: port"
- "channels"
  - "name"
  - "device"
  - "channel"
  - "program: maxvoltage"
  - "program: maxcurrent"

**Required Categories for Chiller labRemote Config:**

- "chillers"
  - "name"
  - "id" (int)
  - "communication: port"
  - "communication: termination"
  - "maxTemp"
  - "minTemp"

#### Under the Hood

The app has the following structure:

- frontend
  - _package.json_
  - _package-lock.json_
  - src
    - _index.js_
    - components
  - _Tabbing.jsx_
  - _Header.jsx_
  - _PowerSupplies.jsx_
  - _Chillers.jsx_
  - _Scripts.jsx_
- backend
  - _main.py_
  - app
    - _**init**.py_
    - _api.py_
  - configs
    - PS\_config.json
    - chiller\_config.json

The Frontend is taking advantage of React libraries. In its src folder, _index.js_ brings all the components together and renders the main display. Each of the JSX components in src/components are independent highly mutable. In particular, Scripts.jsx is an example and can be modified or replaced for the individual lab needs.

The backend relies on FastAPI. In app/_api.py_, requests from interaction with the browser are routed to python commands, where the app utilizes labRemote libraries to communicate with hardware devices. More about this app can be found in [this presentation](https://indico.cern.ch/event/1163020/contributions/4884894/attachments/2450211/4198943/ITk%20labRemote%20GUI%20Status%20052422.pptx%281%29.pdf).

#### Customizations

The App will automatically adjust based on your labRemote config files for varying numbers of chillers and power supply channels. It is necessary to change the chiller initialization in backend/app/api.py to the name of the specific chiller model being used, i.e. replace ".HuberChiller()" with ".whateverChiller()". 

For customizations beyond this, like accessing more specific equipment or the addition of automated custom scripts, it is necessary to modify the source code. Components can be added or modified in frontend/src/components. _Scripts.jsx_ acts as an example for how this can be done:

1. Add necessary functions to backend/app/_api.py_. These can be appended right to the bottom of the script. Here you can import at the top any automation code that you already have written. In most cases, the HTML "GET" operation will be sufficient.
2. Create a new component or modify the _Scripts.jsx_ component to include buttons for the new automation or function that you are adding.
3. Make sure that the new component is included and rendered in frontend/src/_index.js_
