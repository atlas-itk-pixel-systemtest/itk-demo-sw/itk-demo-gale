from __future__ import annotations

import json

import labRemote
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

origins = ["http://localhost:3000", "localhost:3000"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["GET", "POST", "PUT", "DELETE"],
    allow_headers=["*"],
)

PSConfigFileName = "configs/PS_config.json"
PSConfigFile = open(PSConfigFileName)
PS = json.load(PSConfigFile)
chillerConfigFile = open("configs/chiller_config.json")
chillerInfo = json.load(chillerConfigFile)

# Initialize Power Supply Communication and database

hw = labRemote.ec.EquipConf()
hw.setHardwareConfig(PSConfigFileName)


PowerSupplies = []
for chan in PS["channels"]:
    channel = {
        "ps_name": chan["device"],
        "channel_id": chan["channel"],
        "label": chan["name"],
        "maxVoltage": chan["program"]["maxvoltage"],
        "maxCurrent": chan["program"]["maxcurrent"],
        "status": "Off",
        "current": 0.0,
        "voltage": 0.0,
        "current_setpt": 0.0,
        "voltage_setpt": 0.0,
    }
    PowerSupplies.append(channel)

# Initialize Chiller Communication and database

Chills = []
for chill in chillerInfo["chillers"]:
    chiller = {
        "id": chill["id"],
        "name": chill["name"],
        "maxTemp": chill["maxTemp"],
        "minTemp": chill["minTemp"],
        "status": "Off",
        "temperature_setpt": 20.0,
        "temperature": 20.0,
    }
    Chills.append(chiller)
    serial = labRemote.com.TextSerialCom('/dev/ttyUSB0', labRemote.com.SerialCom.BaudRate.Baud9600)
    serial.setTermination(chill['communication']['termination'],"")
    #serial.setTermination("\r\n","")
    chiller = labRemote.chiller.HuberChiller(chill["name"])
    chiller.setCom(serial)
    chiller.init()


@app.get("/", tags=["root"])
async def read_root() -> dict:
    """
    Root GET, initializes Chiller and Power Supply Communication
    """
    serial = labRemote.com.TextSerialCom('/dev/ttyUSB0', labRemote.com.SerialCom.BaudRate.Baud9600)
    serial.setTermination(chill['communication']['termination'],"")
    #serial.setTermination("\r\n","")
    chiller = labRemote.chiller.HuberChiller(chill["name"])
    chiller.setCom(serial)
    chiller.init()

    hw = labRemote.ec.EquipConf()
    hw.setHardwareConfig(PSConfigFileName)

    return {"message": "Welcome to the GALE!"}


@app.get("/ps", tags=["ps"])
async def fetch_all_ps() -> dict:
    """
    Return updated info about power supplies
    """
    for channel in PowerSupplies:
        chan = hw.getPowerSupplyChannel(channel["label"])
        power = chan.getPowerSupply()
        channel["current"] = power.measureCurrent(channel["channel_id"])
        channel["current_setpt"] = power.getCurrentLevel(channel["channel_id"])
        channel["voltage"] = power.measureVoltage(channel["channel_id"])
        channel["voltage_setpt"] = power.getVoltageLevel(channel["channel_id"])
    return {"data": PowerSupplies}


@app.get("/ps/{ps_name}/channel/{channel_id}", tags=["ps"])
async def fetch_single_ps(ps_name: str, channel_id: int) -> dict:
    """
    Return info about a single power supply channel
    """
    for channel in PowerSupplies:
        if channel["ps_name"] == ps_name and channel["channel_id"] == channel_id:
            chan = hw.getPowerSupplyChannel(channel["label"])
            power = chan.getPowerSupply()
            channel["current"] = power.measureCurrent(channel["channel_id"])
            channel["voltage"] = power.measureVoltage(channel["channel_id"])
            return {"data": channel}
    return "Power supply channel with ID was not found"


@app.put("/ps/{ps_name}/channel/{channel_id}/curr_sp/{currSP}", tags=["ps"])
async def setCurrent(ps_name: str, channel_id: int, currSP: float) -> dict:
    """
    Set the current setpoint on a particular power supply channel
    """
    for channel in PowerSupplies:
        if channel["ps_name"] == ps_name and channel["channel_id"] == channel_id:
            chan = hw.getPowerSupplyChannel(channel["label"])
            power = chan.getPowerSupply()
            power.setCurrentLevel(currSP,channel["channel_id"])
            channel["current_setpt"] = currSP
            return {
                "data": f"Power Supply Channel with Power Supply Name {ps_name} and Channel ID {channel_id} has been updated"
            }
    return {
        "data": f"Warning: Power Supply Channel with Power Supply Name {ps_name} and Channel ID {channel_id} could not be found"
    }


@app.put("/ps/{ps_name}/channel/{channel_id}/volt_sp/{volt_sp}", tags=["ps"])
async def setVoltage(ps_name: str, channel_id: int, volt_sp: float) -> dict:
    """
    Set the voltage setpoint on a particular power supply channel
    """
    for channel in PowerSupplies:
        if channel["ps_name"] == ps_name and channel["channel_id"] == channel_id:
            chan = hw.getPowerSupplyChannel(channel["label"])
            power = chan.getPowerSupply()
            power.setVoltageLevel(volt_sp,channel["channel_id"])
            channel["voltage_setpt"] = volt_sp
            return {
                "data": f"Power Supply Channel with Power Supply Name {ps_name} and Channel ID {channel_id} has been updated"
            }
    return {
        "data": f"Warning: Power Supply Channel with Power Supply Name {ps_name} and Channel ID {channel_id} could not be found"
    }


@app.put("/ps/{ps_name}/channel/{channel_id}/on", tags=["ps"])
async def turnOnPS(ps_name: str, channel_id: int) -> dict:
    """
    Turn on specific power supply
    """
    for channel in PowerSupplies:
        if channel["ps_name"] == ps_name and channel["channel_id"] == channel_id:
            chan = hw.getPowerSupplyChannel(channel["label"])
            power = chan.getPowerSupply()
            power.turnOn(channel["channel_id"])
            channel["status"] = "On"
            return {
                "data": f"Power Supply Channel with Power Supply Name {ps_name} and Channel ID {channel_id} has been updated"
            }
    return {
        "data": f"Warning: Power Supply Channel with Power Supply Name {ps_name} and Channel ID {channel_id} could not be found"
    }


@app.put("/ps/{ps_name}/channel/{channel_id}/off", tags=["ps"])
async def turnOffPS(ps_name: str, channel_id: int) -> dict:
    """
    Turn off specific power supply
    """
    for channel in PowerSupplies:
        if channel["ps_name"] == ps_name and channel["channel_id"] == channel_id:
            chan = hw.getPowerSupplyChannel(channel["label"])
            power = chan.getPowerSupply()
            power.turnOff(channel["channel_id"])
            channel["status"] = "Off"
            return {
                "data": f"Power Supply Channel with Power Supply Name {ps_name} and Channel ID {channel_id} has been updated"
            }
    return {
        "data": f"Warning: Power Supply Channel with Power Supply Name {ps_name} and Channel ID {channel_id} could not be found"
    }


@app.get("/chiller", tags=["chill"])
async def fetchAllChillers() -> dict:
    """
    Returns updated info on all chillers
    """
    for chill in Chills:
        #chiller = labRemote.chiller.HuberChiller(chiller["name"])
        chill["temperature"] = chiller.measureTemperature()
    return {"data": Chills}


@app.put("/chiller/{chiller_id}/setpt/{temp_sp}", tags=["chill"])
async def setChillerTemp(chiller_id: int, temp_sp: float) -> dict:
    """
    Set target temperature for specific chiller
    """
    for chill in Chills:
        if chill["id"] == chiller_id:
            #chiller = labRemote.chiller.HuberChiller(chiller["name"])
            chiller.setTargetTemperature(temp_sp)
            chill["temperature_setpt"] = temp_sp
            return {"data": f"Chiller with Chiller ID {chiller_id} has been updated"}
    return {"data": f"Warning: Chiller with Chiller ID {chiller_id} could not be found"}


@app.put("/chiller/{chiller_id}/on", tags=["chill"])
async def turnOnChiller(chiller_id: int) -> dict:
    """
    Turn on circulation and temperature control for specific chiller
    """
    for chill in Chills:
        if chill["id"] == chiller_id:
            #chiller = labRemote.chiller.HuberChiller(chiller["name"])
            chiller.turnOn()
            chill["status"] = "On"
            return {"data": f"Chiller with Chiller ID {chiller_id} has been updated"}
    return {"data": f"Warning: Chiller with Chiller ID {chiller_id} could not be found"}


@app.put("/chiller/{chiller_id}/off", tags=["chill"])
async def turnOffChiller(chiller_id: int) -> dict:
    """
    Turn off circulation and temperature control for specific chiller
    """
    for chill in Chills:
        if chill["id"] == chiller_id:
            #chiller = labRemote.chiller.HuberChiller(chiller["name"])
            chiller.turnOff()
            chill["status"] = "Off"
            return {"data": f"Chiller with Chiller ID {chiller_id} has been updated"}
    return {"data": f"Warning: Chiller with Chiller ID {chiller_id} could not be found"}


PSConfigFile.close()
chillerConfigFile.close()
