import React, { useEffect, useState, useRef } from "react";

import {
  Box,
  Button,
  Flex,
  Input,
  InputGroup,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Stack,
  Text,
  Slider,
  SliderTrack,
  SliderFilledTrack,
  SliderThumb,
  SliderMark,
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  NumberIncrementStepper,
  NumberDecrementStepper,
  useDisclosure,
} from "@chakra-ui/core";

export default function Scripts() {
  return (
    <>
      <h2>Examples</h2>
      <Stack spacing={10}>
        <Button maxW="500px">Initiate Thermal Cycling</Button>
        <Button maxW="500px">Initiate Sensor IV Scan</Button>
        <Flex align="end">
          <Slider maxW="250px" min={-55} max={50} step={1}>
            <SliderTrack>
              <SliderFilledTrack />
            </SliderTrack>
            <SliderThumb />
          </Slider>
          <Text> C</Text>
          <Button>Use TEC to Ramp Module to Temperature</Button>
        </Flex>
      </Stack>
    </>
  );
}
