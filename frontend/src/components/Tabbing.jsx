import React from "react";
import { render } from "react-dom";
import { ThemeProvider } from "@chakra-ui/core";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import { Link, Route, Routes } from "react-router-dom";

import Home from "./Home";
import PowerSupplies from "./PowerSupplies";
import Chillers from "./Chillers";
import Scripts from "./Scripts";

import style from "./Tabbing.module.css"

export default function Tabbing() {
	return (
		<div>
		<nav>
		  <ul className={style.tabs}>
		    <li className={style.tab}>
		      <Link to="/" className={style.tabContent}>Home</Link>
		    </li>
		    <li className={style.tab}>
		      <Link to="/chillercontrol" className={style.tabContent}>Chiller Control</Link>
		    </li>
		    <li className={style.tab}>
		      <Link to="/powercontrol"className={style.tabContent}>Power Supply Control</Link>
		    </li>
		    <li className={style.tab}>
		      <Link to="/scripts" className={style.tabContent}>Custom Scripts</Link>
		    </li>
		  </ul>
		</nav>
		
		<Routes>
		  <Route path="/" element={<Home />} />
		  <Route path="/chillercontrol" element={<Chillers />} />
		  <Route path="/powercontrol" element={<PowerSupplies />} />
		  <Route path="/scripts" element={<Scripts />} />
		</Routes>
		</div>
	);
}

/*export default function Tabbing() {
  return (
    <Tabs>
      <TabList>
        <Tab>Chiller Control</Tab>
        <Tab>Power Supply Control</Tab>
        <Tab>Custom Scripts</Tab>
      </TabList>

      <TabPanel>
        <Chillers />
      </TabPanel>
      <TabPanel>
        <PowerSupplies />
      </TabPanel>
      <TabPanel>
        <Scripts />
      </TabPanel>
    </Tabs>
  );
}*/
