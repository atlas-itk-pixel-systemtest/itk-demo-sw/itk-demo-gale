import React, { useEffect, useState, useRef } from "react";
import {
  Box,
  Button,
  Flex,
  Input,
  InputGroup,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Stack,
  Text,
  Slider,
  SliderTrack,
  SliderFilledTrack,
  SliderThumb,
  SliderMark,
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  NumberIncrementStepper,
  NumberDecrementStepper,
  SimpleGrid,
  useDisclosure,
} from "@chakra-ui/core";
import style from "./PowerSupplies.module.css"

const PSContext = React.createContext({
  PS: [],
  fetchPS: () => {},
});

export default function PowerSupplies() {
  const [PS, setPS] = useState([]);

  const fetchPS = async () => {
    const response = await fetch("http://localhost:8000/ps");
    const PS = await response.json();
    setPS(PS.data);
  };
  useEffect(() => {
    fetchPS();
    var psInterval = setInterval(() => {
      	fetchPS();
    }, 1000);
    return () => {
	    clearInterval(psInterval);
    }
  }, []);
  return (
    <PSContext.Provider value={{ PS, fetchPS }}>
      <hr></hr>
      <h1 style={{textAlign:"center"}}>Power Supply Control</h1>
      <hr></hr>
      <SimpleGrid minChildWidth="400px" spacing={5}>
        {PS.map((ps) => (
          <Box p="10px" border="4px" borderColor="purple.600" bg="yellow.100">
            <h4 style={{textAlign:"center"}}>{ps.ps_name} - Channel {ps.channel_id} - {ps.label}</h4>
            <div className={style.content}>
	      <div className={style.contentLabel}>Status: </div>
	      <div className={style.numberContent}>{ps.status}</div>
	    </div>
	    <Flex align="end">
	      <div className={style.content}>
                <div className={style.contentLabel}>Current Setpoint: </div>
	        <div className={style.numberContent}>{ps.current_setpt}</div>
	      </div>
	      <div className={style.content}>
                <div className={style.contentLabel}>Current Output: </div>
                <div className={style.numberContent}>{ps.current}</div>
              </div>
	    </Flex>
	    <Flex align="end">
	      <div className={style.content}>
                <div className={style.contentLabel}>Voltage Setpoint: </div>
	        <div className={style.numberContent}>{ps.voltage_setpt}</div>
	      </div>
	      <div className={style.content}>
                <div className={style.contentLabel}>Voltage Output: </div>
                <div className={style.numberContent}>{ps.voltage}</div>
              </div>
	    </Flex>
            <TogglePower
              ps_name={ps.ps_name}
              chan_id={ps.channel_id}
              prev_status={ps.status}
            />
            <SetCurrent
              ps_name={ps.ps_name}
              chan_id={ps.channel_id}
              prev_curr_sp={ps.current_setpt}
              maxCurrent={ps.maxCurrent}
            />
            <SetVoltage
              ps_name={ps.ps_name}
              chan_id={ps.channel_id}
              prev_volt_sp={ps.voltage_setpt}
              maxVoltage={ps.maxVoltage}
            />
          </Box>
        ))}
      </SimpleGrid>
    </PSContext.Provider>
  );
}

function TogglePower({ ps_name, chan_id, prev_status }) {
  const [stat, setStat] = useState(prev_status);
  const { fetchPS } = React.useContext(PSContext);

  const turnOn = async () => {
    setStat("On");
    console.log(stat);
    await fetch(`http://localhost:8000/ps/${ps_name}/channel/${chan_id}/on`, {
      method: "PUT",
    });
    await fetchPS();
  };

  const turnOff = async () => {
    setStat("Off");
    console.log(stat);
    await fetch(`http://localhost:8000/ps/${ps_name}/channel/${chan_id}/off`, {
      method: "PUT",
    });
    await fetchPS();
  };

  return (
    <Flex align="end" style={{justifyContent:"center"}}>
      <Button bg="purple.200" style={{marginRight:"20px"}} onClick={turnOn}>
        On
      </Button>
      <Button bg="purple.200" onClick={turnOff}>
        Off
      </Button>
    </Flex>
  );
}

function SetCurrent({ ps_name, chan_id, prev_curr_sp, maxCurrent }) {
  const [currSP, setCurrSP] = useState(prev_curr_sp);
  const { fetchPS } = React.useContext(PSContext);

  const handleCurrSPChange = (value) => {
    setCurrSP(value);
    console.log(value);
  };

  const setCurrent = async () => {
    console.log(currSP);
    await fetch(
      `http://localhost:8000/ps/${ps_name}/channel/${chan_id}/curr_sp/${currSP}`,
      {
        method: "PUT",
      }
    );
    await fetchPS();
  };

  return (
    <Flex align="end" style={{justifyContent:"center"}}>
      <Slider
        maxW="250px"
	style={{marginRight:"5px"}}
        min={0.0}
        max={maxCurrent}
        step={maxCurrent / 50}
        onChange={(val) => handleCurrSPChange(val)}
      >
        <SliderTrack bg="purple.200">
          <SliderFilledTrack bg="purple.600" />
        </SliderTrack>
        <SliderThumb />
      </Slider>
      <div style={{marginRight:"15px"}}>{currSP} A </div>
      <Button bg="purple.200" onClick={setCurrent}>
        Set Current
      </Button>
    </Flex>
  );
}

function SetVoltage({ ps_name, chan_id, prev_volt_sp, maxVoltage }) {
  const [voltSP, setVoltSP] = useState(prev_volt_sp);
  const { fetchPS } = React.useContext(PSContext);

  const handleVoltSPChange = (value) => {
    setVoltSP(value);
    console.log(value);
  };

  const setVoltage = async () => {
    console.log(voltSP);
    await fetch(
      `http://localhost:8000/ps/${ps_name}/channel/${chan_id}/volt_sp/${voltSP}`,
      {
        method: "PUT",
      }
    );
    await fetchPS();
  };

  return (
    <Flex align="end" style={{justifyContent:"center"}}>
      <Slider
        maxW="250px"
	style={{marginRight:"5px"}}
        min={0.0}
        max={maxVoltage}
        step={maxVoltage / 50}
        onChange={(val) => handleVoltSPChange(val)}
      >
        <SliderTrack bg="purple.200">
          <SliderFilledTrack bg="purple.600" />
        </SliderTrack>
        <SliderThumb />
      </Slider>
      <div style={{marginRight:"15px"}}>{voltSP} V </div>
      <Button bg="purple.200" fontColor="yellow.100" onClick={setVoltage}>
        Set Voltage
      </Button>
    </Flex>
  );
}
