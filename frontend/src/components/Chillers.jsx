import React, { useEffect, useState, useRef } from "react";
import {
  Box,
  Button,
  Flex,
  Input,
  InputGroup,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Stack,
  Text,
  Slider,
  SliderTrack,
  SliderFilledTrack,
  SliderThumb,
  SliderMark,
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  NumberIncrementStepper,
  NumberDecrementStepper,
  useDisclosure,
} from "@chakra-ui/core";
import style from "./Chillers.module.css"

const ChillerContext = React.createContext({
  chillers: [],
  fetchChillers: () => {},
});

export default function Chillers() {
  const [chillers, setChillers] = useState([]);
  const fetchChillers = async () => {
    const response = await fetch("http://localhost:8000/chiller");
    const chillers = await response.json();
    setChillers(chillers.data);
  };
  useEffect(() => {
    fetchChillers();
    var chillInterval = setInterval(() => {
      fetchChillers();
    }, 5000);
    return () => {
	    clearInterval(chillInterval);
    }
  }, []);
  return (
    <ChillerContext.Provider value={{ chillers, fetchChillers }}>
      <hr></hr>
      <h1 style={{textAlign: "center"}}>Chiller Control</h1>
      <hr></hr>
      <Stack spacing={5}>
        {chillers.map((chiller) => (
          <Box bg="green.100" p="10px" w='50%' border="4px" borderColor="blue.600" className={style.center}>
            <h3 style={{textAlign: "center"}}>{chiller.name}</h3>
	    <div className={style.content}>
              <div className={style.contentLabel}>ID: </div>
	      <div className={style.numberContent}>{chiller.id}</div>
	    </div>
	    <div className={style.content}>
              <div className={style.contentLabel}>Status: </div>
	      <div className={style.numberContent}>{chiller.status}</div>
	    </div>
	    <div className={style.content}>
              <div className={style.contentLabel}>Setpoint Temperature: </div>
	      <div className={style.numberContent}>{chiller.temperature_setpt}</div>
	    </div>
	    <div className={style.content}>
              <div className={style.contentLabel}>Internal Temperature: </div>
	      <div className={style.numberContent}>{chiller.temperature}</div>
	    </div>
            <TogglePower chill_id={chiller.id} prev_status={chiller.status} />
            <SetTemperature
              chill_id={chiller.id}
              prev_temp_sp={chiller.temperature_setpt}
              minTemp={chiller.minTemp}
              maxTemp={chiller.maxTemp}
            />
          </Box>
        ))}
      </Stack>
    </ChillerContext.Provider>
  );
}

function TogglePower({ chill_id, prev_status }) {
  const [stat, setStat] = useState(prev_status);
  const { fetchChillers } = React.useContext(ChillerContext);

  const turnOn = async () => {
    setStat("On");
    console.log(stat);
    await fetch(`http://localhost:8000/chiller/${chill_id}/on`, {
      method: "PUT",
    });
    await fetchChillers();
  };

  const turnOff = async () => {
    setStat("Off");
    console.log(stat);
    await fetch(`http://localhost:8000/chiller/${chill_id}/off`, {
      method: "PUT",
    });
    await fetchChillers();
  };

  return (
    <Flex style={{justifyContent:"center"}} align="end">
      <Button bg="blue.200" style={{marginRight:"20px"}} onClick={turnOn}>
        On
      </Button>
      <Button bg="blue.200" onClick={turnOff}>
        Off
      </Button>
    </Flex>
  );
}

function SetTemperature({ chill_id, prev_temp_sp, minTemp, maxTemp }) {
  const [tempSP, setTempSP] = useState(prev_temp_sp);
  const { fetchChillers } = React.useContext(ChillerContext);

  const handleTempSPChange = (value) => {
    setTempSP(value);
    console.log(value);
  };

  const setTemperature = async () => {
    console.log(tempSP);
    await fetch(`http://localhost:8000/chiller/${chill_id}/setpt/${tempSP}`, {
      method: "PUT",
    });
    await fetchChillers();
  };

  return (
    <Flex align="end" style={{justifyContent:"center"}}>
      <Slider
        maxW="350px"
	style={{marginRight:"5px"}}
        min={minTemp}
        max={maxTemp}
        step={0.1}
        onChange={(val) => handleTempSPChange(val)}
      >
        <SliderTrack bg="blue.200">
          <SliderFilledTrack bg="blue.600" />
        </SliderTrack>
        <SliderThumb />
      </Slider>
      <div style={{marginRight:"15px"}}>{tempSP} C </div>
      <Button bg="blue.200" onClick={setTemperature}>
        Set Target Temperature
      </Button>
    </Flex>
  );
}
